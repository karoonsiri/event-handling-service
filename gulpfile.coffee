bower         = require 'bower'
gulp          = require 'gulp'
gutil         = require 'gulp-util'
concat        = require 'gulp-concat'
sass          = require 'gulp-sass'
rename        = require 'gulp-rename'
coffee        = require 'gulp-coffee'
uglify        = require 'gulp-uglify'
watch         = require 'gulp-watch'

paths = {
  lib: [
    './libs/jquery/dist/jquery.js',
    './libs/angular/angular.js',
    './libs/jPlayer/dist/jplayer/jquery.jplayer.js',
    './libs/bootstrap/dist/js/bootstrap.js'
  ]
  sass: ['./scss/**/*.scss']
  app:  ['./app/**/*.coffee']
}

gulp.task 'watch', ->
  watch paths.lib, -> gulp.start ['library']
  watch paths.sass, -> gulp.start ['sass']
  watch paths.app, -> gulp.start ['appjs']

gulp.task 'default', ['build', 'watch']

gulp.task 'build', ['sass', 'appjs', 'library']

gulp.task 'sass', (done) ->
  gulp.src paths.sass
    .pipe sass()
    .pipe concat('style.css')
    .pipe gulp.dest('./www/css/')
    .on('end', done)

  # Just return to avoid CoffeeScript's implicit returning the stream
  return

gulp.task 'appjs', ->
  gulp.src paths.app
    .pipe coffee()
      .on 'error', gutil.log
    .pipe concat 'application.js'
    .pipe gulp.dest './www/js'
  return

gulp.task 'bootstrap', ->
  gulp.src './libs/bootstrap/dist/css/*'
    .pipe gulp.dest './www/bootstrap/css/'

  gulp.src './libs/bootstrap/dist/js/*'
    .pipe gulp.dest './www/bootstrap/js'

  gulp.src './libs/bootstrap/dist/fonts/*'
    .pipe gulp.dest './www/bootstrap/fonts'

  return

gulp.task 'library', ->
  gulp.src paths.lib
    .pipe concat 'libraries.js'
    .pipe uglify()
    .pipe gulp.dest './www/js'
