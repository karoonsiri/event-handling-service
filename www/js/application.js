(function() {
  angular.module('DemoApp', []);

}).call(this);

(function() {
  var playerCtrl;

  playerCtrl = function($scope, $interval, Playlist, PlayerEventListener) {
    $scope.init = function() {
      var interval;
      $scope.player = $("#player_container").jPlayer({
        solution: "html",
        supplied: "mp3,m4a,wav,mp4,ogg,webm,mov",
        volume: 1.0,
        play: PlayerEventListener.played,
        pause: PlayerEventListener.paused,
        timeupdate: PlayerEventListener.progressing
      });
      return interval = $interval(function() {
        return $scope.percent = $scope.globalPercent;
      }, 250);
    };
    $scope.selectSong = function(index) {
      $($scope.player).jPlayer('setMedia', {
        mp3: Playlist.songs[Playlist.currentSong].url
      });
      $($scope.player).jPlayer('play');
    };
    $scope.play = function() {
      if (!Playlist.currentSong) {
        Playlist.setFirst();
        $scope.selectSong(Playlist.currentSong);
      }
      $($scope.player).jPlayer('play');
    };
    $scope.pause = function() {
      $($scope.player).jPlayer('pause');
    };
    $scope.togglePlay = function() {
      if ($scope.playing) {
        $scope.pause();
      } else {
        $scope.play();
      }
    };
    $scope.nextSong = function() {
      if (!Playlist.currentSong) {
        Playlist.setFirst();
      }
      Playlist.currentSong += 1;
      return $scope.selectSong(Playlist.currentSong);
    };
    $scope.previousSong = function() {
      if (!Playlist.currentSong) {
        Playlist.setFirst();
      }
      Playlist.currentSong -= 1;
      return $scope.selectSong(Playlist.currentSong);
    };
    return $scope.init();
  };

  playerCtrl.$inject = ['$scope', '$interval', 'Playlist', 'PlayerEventListener'];

  angular.module('DemoApp').controller('PlayerCtrl', playerCtrl);

}).call(this);

(function() {
  var playerEventListener;

  playerEventListener = function($rootScope) {
    var _service, paused, played, progressing;
    played = function(e) {
      $rootScope.playing = true;
      return $rootScope.$apply();
    };
    paused = function(e) {
      $rootScope.playing = false;
      return $rootScope.$apply();
    };
    progressing = function(e) {
      var percent;
      percent = e.jPlayer.status.currentPercentAbsolute;
      return $rootScope.globalPercent = percent;
    };
    _service = {
      played: played,
      paused: paused,
      progressing: progressing
    };
    return _service;
  };

  playerEventListener.$inject = ['$rootScope'];

  angular.module('DemoApp').service('PlayerEventListener', playerEventListener);

}).call(this);

(function() {
  var playlistCtrl;

  playlistCtrl = function($scope, Playlist) {
    $scope.songs = function() {
      return Playlist.songs;
    };
    $scope.init = function() {};
    return $scope.init();
  };

  playlistCtrl.$inject = ['$scope', 'Playlist'];

  angular.module('DemoApp').controller('PlaylistCtrl', playlistCtrl);

}).call(this);

(function() {
  var playlistService;

  playlistService = function() {
    var _service;
    _service = {
      songs: [
        {
          title: 'Bubble',
          url: 'http://www.miaowmusic.net/mp3/Miaow-07-Bubble.mp3'
        }, {
          title: 'Beside Me',
          url: 'http://www.miaowmusic.net/mp3/Miaow-06-Beside-me.mp3'
        }, {
          title: 'Stirring Of A Fool',
          url: 'http://www.miaowmusic.net/mp3/Miaow-08-Stirring-of-a-fool.mp3'
        }
      ],
      currentSong: void 0,
      setFirst: function() {
        return _service.currentSong = 0;
      }
    };
    return _service;
  };

  angular.module('DemoApp').service('Playlist', playlistService);

}).call(this);
