playerEventListener = ($rootScope) ->
  played = (e) ->
    $rootScope.playing = true
    $rootScope.$apply()

  paused = (e) ->
    $rootScope.playing = false
    $rootScope.$apply()

  progressing = (e) ->
    percent = e.jPlayer.status.currentPercentAbsolute
    $rootScope.globalPercent = percent

  _service = {
    played: played
    paused: paused
    progressing: progressing
  }

  return _service

playerEventListener.$inject = ['$rootScope']
angular.module('DemoApp').service 'PlayerEventListener', playerEventListener
