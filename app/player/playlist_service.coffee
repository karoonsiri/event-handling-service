playlistService = ->

  _service = {
    songs: [
      { title: 'Bubble', url: 'http://www.miaowmusic.net/mp3/Miaow-07-Bubble.mp3' },
      { title: 'Beside Me', url: 'http://www.miaowmusic.net/mp3/Miaow-06-Beside-me.mp3' },
      { title: 'Stirring Of A Fool', url: 'http://www.miaowmusic.net/mp3/Miaow-08-Stirring-of-a-fool.mp3' }
    ]
    currentSong: undefined,
    setFirst: ->
      _service.currentSong = 0
  }

  return _service

angular.module('DemoApp').service 'Playlist', playlistService
