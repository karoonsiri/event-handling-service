playerCtrl = ($scope, $interval, Playlist, PlayerEventListener) ->
  $scope.init = ->
    $scope.player = $("#player_container").jPlayer {
      # constructor options
      solution: "html",
      supplied: "mp3,m4a,wav,mp4,ogg,webm,mov",

      # default value
      volume: 1.0

      # Event handlers
      play: PlayerEventListener.played,
      pause: PlayerEventListener.paused,
      # setmedia: PlayerEventListener.setMedia,
      timeupdate: PlayerEventListener.progressing,
      # error: PlayerEvent.error,
    }

    interval = $interval(
      ->
        $scope.percent = $scope.globalPercent
      ,
      250
    )

  $scope.selectSong = (index) ->
    $($scope.player).jPlayer 'setMedia', {
      mp3: Playlist.songs[Playlist.currentSong].url
    }
    $($scope.player).jPlayer 'play'
    return

  $scope.play = ->
    unless Playlist.currentSong
      Playlist.setFirst()
      $scope.selectSong(Playlist.currentSong)

    $($scope.player).jPlayer 'play'
    return

  $scope.pause = ->
    $($scope.player).jPlayer 'pause'
    return

  $scope.togglePlay = ->
    if $scope.playing
      $scope.pause()
    else
      $scope.play()

    return

  $scope.nextSong = ->
    unless Playlist.currentSong
      Playlist.setFirst()

    Playlist.currentSong += 1
    $scope.selectSong(Playlist.currentSong)

  $scope.previousSong = ->
    unless Playlist.currentSong
      Playlist.setFirst()

    Playlist.currentSong -= 1
    $scope.selectSong(Playlist.currentSong)

  $scope.init()

playerCtrl.$inject = ['$scope', '$interval', 'Playlist', 'PlayerEventListener']
angular.module('DemoApp').controller 'PlayerCtrl', playerCtrl
